import React from 'react'
import { useForm } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'
import schema from '../../validation'
import './style.css';

export default function Profile() {
  const {
    register,
    handleSubmit,
    formState: { errors }
  } = useForm({
    resolver: yupResolver(schema)
  })
  const onSubmit = (data) => console.log(data)

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <div>
        <input type="text" placeholder='Nombres'{...register('firstName')} />
        {errors.firstName && <p>{errors.firstName.message}</p>}
      </div>
      <div style={{ marginBottom: 10 }}>
        <input type="text" placeholder='Apellidos'{...register('lastName')} />
        {errors.lastName && <p>{errors.lastName.message}</p>}
      </div>
      <div>
        <input type="text" placeholder='Correo Electrónico Corporativo'{...register('email')} />
        {errors.email && <p>{errors.email.message}</p>}
      </div>
      <div>
        <input type="password" placeholder='Ingresa una contraseña'{...register('password')} />
        {errors.password && <p>{errors.password.message}</p>}
      </div>
      <div>
        <input type="password" placeholder='Confirma la contraseña'{...register('password')} />
        {errors.password1 && <p>{errors.password1.message}</p>}
      </div>

      <div>
        <h2 className='inform'>¿Cómo nos conociste?</h2>
      </div>

      <div>
        <input type='radio' id='advisor' name="gender" value='advisor' />
        <label htmlFor='advisor' className='advisor'>Asesor Comercial</label>
        <input type='radio' id='socialMedia' name="gender" value='socialMedia' />
        <label htmlFor='socialMedia' className='socialMedia'>Redes Sociales</label>
        <input type='radio' id='other' name="gender" value='other' />
        <label htmlFor='other' className='other'>Otro</label>
      </div>

      <input type='submit' value='Comenzar registro' />

      <div>
        <h2 className='inform1'>¿Ya tienes cuenta?Inicia sesión</h2>
      </div>

      <input type='submit' value='Regístrate aquí' />

      <div>
        <p className='security'>Los datos proveidos están seguros Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit au</p>
      </div>

    </form>
  )
}
