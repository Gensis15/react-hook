import { IS_DEVELOP } from './constants';

export const LogManager = {
  log: msg => IS_DEVELOP && console.log(msg),

  info(msg) {
    if (IS_DEVELOP) console.info(msg);
  },

  warn(msg) {
    if (IS_DEVELOP) console.warn(msg);
  },

  error(msg) {
    if (IS_DEVELOP) console.error(msg);
  }
};
