const URL_ENVIRONMENTS = {
  Public: '',
  BanorteDev: 'https://d-banorte-service.novopayment.net/',
  BanorteTest: 'https://t-banorte-service.novopayment.net/',
  BanorteUat: 'https://uat-banorte-service.novopayment.net/',
  BanorteProd: 'https://banorte-service.novopayment.net/',
  CpmProd: 'https://web-service.novopayment.net/'
};

export const URL_CURRENT = URL_ENVIRONMENTS.BanorteProd;
export const IS_DEVELOP = /^https:\/\/d-/.test(URL_CURRENT);
export const IS_DEBUG_MODE = true;
